"
 (C) 2010-2011 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

Integer extend [
    swap16 [
	| tmp |
        <category: '*OsmoNetwork-Message'>

	tmp := self bitAnd: 16rFFFF.
	^ (tmp bitShift: -8) bitOr: ((tmp bitAnd: 16rFF) bitShift: 8)
    ]

    swap32 [
        | tmp |
        "Certainly not the most effective way"
        <category: '*OsmoNetwork-Message'>

        tmp := 0.
        tmp := tmp bitOr: ((self bitAnd: 16rFF000000) bitShift: -24).
        tmp := tmp bitOr: ((self bitAnd: 16r00FF0000) bitShift: -8).
        tmp := tmp bitOr: ((self bitAnd: 16r0000FF00) bitShift:  8).
        tmp := tmp bitOr: ((self bitAnd: 16r000000FF) bitShift: 24).

        ^ tmp
    ]
]

Object extend [
    toMessage [
        | msg |
        <category: '*OsmoNetwork-message'>
        msg := Osmo.MessageBuffer new.
        self writeOn: msg.
        ^ msg
    ]

    toMessageOrByteArray [
        <category: '*OsmoNetwork-Message'>
        ^ self toMessage
    ]
]

ByteArray extend [
    toMessageOrByteArray [
        <category: '*OsmoNetwork-Message'>
        ^ self
    ]
]


"Code from FileDescriptor, GST license"
Sockets.Socket extend [
    nextUshort [
	"Return the next 2 bytes in the byte array, interpreted as a 16 bit unsigned int"
        <category: '*OsmoNetwork-Message'>
	^self nextBytes: 2 signed: false
    ]

    nextBytes: n signed: signed [
	"Private - Get an integer out of the next anInteger bytes in the stream"

        <category: '*OsmoNetwork-Message'>
	| int msb |
	int := 0.
	0 to: n * 8 - 16
	    by: 8
	    do: [:i | int := int + (self nextByte bitShift: i)].
	msb := self nextByte.
	(signed and: [msb > 127]) ifTrue: [msb := msb - 256].
	^int + (msb bitShift: n * 8 - 8)
    ]

    nextByte [
	"Return the next byte in the file, or nil at eof"

        <category: '*OsmoNetwork-Message'>
	| a |
	a := self next.
	^a isNil ifTrue: [a] ifFalse: [a asInteger]
    ]
]

Sockets.StreamSocket extend [
    nextUshort [
	"Return the next 2 bytes in the byte array, interpreted as a 16 bit unsigned int"
        <category: '*OsmoNetwork-Message'>
	^self nextBytes: 2 signed: false
    ]

    nextBytes: n signed: signed [
	"Private - Get an integer out of the next anInteger bytes in the stream"

        <category: '*OsmoNetwork-Message'>
	| int msb |
	int := 0.
	0 to: n * 8 - 16
	    by: 8
	    do: [:i | int := int + (self nextByte bitShift: i)].
	msb := self nextByte.
	(signed and: [msb > 127]) ifTrue: [msb := msb - 256].
	^int + (msb bitShift: n * 8 - 8)
    ]

    nextByte [
	"Return the next byte in the file, or nil at eof"

        <category: '*OsmoNetwork-Message'>
	| a |
	a := self next.
	^a isNil ifTrue: [a] ifFalse: [a asInteger]
    ]
]

