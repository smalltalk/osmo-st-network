"
 (C) 2013 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

BlockClosure extend [
    value: arg1 value: arg2 value: arg3 value: arg4 [
        <category: '*OsmoNetwork'>
	"Evaluate the receiver passing arg1, arg2, arg3 and arg4 as the parameters"

	<category: 'built ins'>
	<primitive: VMpr_BlockClosure_value>
	SystemExceptions.WrongArgumentCount signal
    ]
]
