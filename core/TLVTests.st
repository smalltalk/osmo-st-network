"
 (C) 2012,2014 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

TestCase subclass: TLVDescriptionTest [
    <category: 'OsmoNetwork-Tests'>
    <comment: 'I try to test the TLV Description'>

    testTLVCreation [
        | tlv |

        "Test default"
        tlv := TLVDescription new.
        self
            assert: tlv isMandatory;
            deny: tlv isOptional.

        "Test update"
        tlv presenceKind: tlv class optional.
        self
            assert: tlv isOptional;
            deny: tlv isMandatory.

        tlv instVarName: #bla.
        self assert: tlv instVarName = #bla.

        tlv tag: 16r23.
        self assert: tlv tag = 16r23.

        tlv beLV.
        self
            assert: tlv typeKind equals: #lv;
            assert: tlv hasLength;
            deny: tlv hasTag.

        tlv beTLV.
        self
            assert: tlv typeKind equals: #tlv;
            assert: tlv hasLength;
            assert: tlv hasTag.

        tlv beTagOnly.
        self
            assert: tlv typeKind equals: #tagOnly;
            assert: tlv hasTag;
            deny: tlv hasLength.
    ]

    testNeedsTag [
        | tlv |
        tlv := TLVDescription new
            tag: 16r23;
            beTV;
            beConditional;
            yourself.

        self assert: tlv needsTag.
    ]
]
