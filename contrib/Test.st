"
 (C) 2010 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"
"Test code"

Eval [
    | socket muxer demuxer data dispatcher ipa |
    FileStream fileIn: 'Message.st'.
    FileStream fileIn: 'IPAMuxer.st'.
    FileStream fileIn: 'IPAConstants.st'.
    FileStream fileIn: 'IPADispatcher.st'.
    FileStream fileIn: 'IPAProtoHandler.st'.
    FileStream fileIn: 'Extensions.st'.

    socket := Sockets.Socket remote: '127.0.0.1' port: 5000.
    demuxer := IPADemuxer initOn: socket.
    muxer := IPAMuxer initOn: socket.

    dispatcher := IPADispatcher new.

    ipa := IPAProtoHandler new.
    dispatcher addHandler: IPAConstants protocolIPA on: ipa with: #handleMsg:.
    dispatcher addHandler: IPAConstants protocolMGCP on: ipa with: #handleNoop:.

    [true] whileTrue: [
	[
            data := demuxer next.
            data inspect.
            dispatcher dispatch: data first with: data second.
	] on: SystemExceptions.EndOfStream do: [:e | ^ false ].
    ]
]
