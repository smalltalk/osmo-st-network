"
 (C) 2011-2012 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

Object subclass: UAConstants [
    "
    "

    <category: 'OsmoNetwork-M2UA'>
    <comment: 'I hold the mapping from M2UA constants to their
    numeric representation. The following classes are defined:

    Management (MGMT) Message [IUA/UA/M3UA/SUA]
    Transfer Messages [M3UA]
    SS7 Signalling Network Management (SSNM) Messages [M3UA/SUA] 
    ASP State Maintenance (ASPSM) Messages [IUA/UA/M3UA/SUA]
    ASP Traffic Maintenance (ASPTM) Messages [IUA/UA/M3UA/SUA]
    Q.921/Q.931 Boundary Primitives Transport (QPTM)
    MTP2 User Adaptation (MAUP) Messages [UA]
    Connectionless Messages [SUA]
    Connection-Oriented Messages [SUA]
    Routing Key Management (RKM) Messages (M3UA)
    Interface Identifier Management (IIM) Messages (UA)
        '>

    UAConstants class >> clsMgmt            [ <category: 'constants'> ^ 0  ]
    UAConstants class >> clsTrans           [ <category: 'constants'> ^ 1  ]
    UAConstants class >> clsSSMN            [ <category: 'constants'> ^ 2  ]
    UAConstants class >> clsASPSM           [ <category: 'constants'> ^ 3  ]
    UAConstants class >> clsASPTM           [ <category: 'constants'> ^ 4  ]
    UAConstants class >> clsQPTM            [ <category: 'constants'> ^ 5  ]
    UAConstants class >> clsMAUP            [ <category: 'constants'> ^ 6  ]
    UAConstants class >> clsSUA_LESS        [ <category: 'constants'> ^ 7  ]
    UAConstants class >> clsSUA_CONN        [ <category: 'constants'> ^ 8  ]
    UAConstants class >> clsRKM             [ <category: 'constants'> ^ 9  ]
    UAConstants class >> clasIIM            [ <category: 'constants'> ^ 10 ]


    UAConstants class >> maupReserved       [ <category: 'constants'> ^ 0  ]
    UAConstants class >> maupData           [ <category: 'constants'> ^ 1  ]
    UAConstants class >> maupEstReq         [ <category: 'constants'> ^ 2  ]
    UAConstants class >> maupEstCon         [ <category: 'constants'> ^ 3  ]
    UAConstants class >> maupRelReq         [ <category: 'constants'> ^ 4  ]
    UAConstants class >> maupRelCon         [ <category: 'constants'> ^ 5  ]
    UAConstants class >> maupRelInd         [ <category: 'constants'> ^ 6  ]
    UAConstants class >> maupStateReq       [ <category: 'constants'> ^ 7  ]
    UAConstants class >> maupStateCon       [ <category: 'constants'> ^ 8  ]
    UAConstants class >> maupStateInd       [ <category: 'constants'> ^ 9  ]
    UAConstants class >> maupDRetrReq       [ <category: 'constants'> ^ 10 ]
    UAConstants class >> maupDRetrCon       [ <category: 'constants'> ^ 11 ]
    UAConstants class >> maupDRetrInd       [ <category: 'constants'> ^ 12 ]
    UAConstants class >> maupDRetrCompl     [ <category: 'constants'> ^ 13 ]
    UAConstants class >> maupCongInd        [ <category: 'constants'> ^ 14 ]
    UAConstants class >> maupDataAck        [ <category: 'constants'> ^ 15 ]

    UAConstants class >> aspsmReserved      [ <category: 'constants'> ^ 0  ]
    UAConstants class >> aspsmUp            [ <category: 'constants'> ^ 1  ]
    UAConstants class >> aspsmDown          [ <category: 'constants'> ^ 2  ]
    UAConstants class >> aspsmBeat          [ <category: 'constants'> ^ 3  ]
    UAConstants class >> aspsmUpAck         [ <category: 'constants'> ^ 4  ]
    UAConstants class >> aspsmDownAck       [ <category: 'constants'> ^ 5  ]
    UAConstants class >> aspsmBeatAck       [ <category: 'constants'> ^ 6  ]

    UAConstants class >> asptmReserved      [ <category: 'constants'> ^ 0  ]
    UAConstants class >> asptmActiv         [ <category: 'constants'> ^ 1  ]
    UAConstants class >> asptmInactiv       [ <category: 'constants'> ^ 2  ]
    UAConstants class >> asptmActivAck      [ <category: 'constants'> ^ 3  ]
    UAConstants class >> asptmInactivAck    [ <category: 'constants'> ^ 4  ]

    UAConstants class >> mgmtError          [ <category: 'constants'> ^ 0  ]
    UAConstants class >> mgmtNtfy           [ <category: 'constants'> ^ 1  ]

    UAConstants class >> iimReserved        [ <category: 'constants'> ^ 0  ]
    UAConstants class >> iimRegReq          [ <category: 'constants'> ^ 1  ]
    UAConstants class >> iimRegRsp          [ <category: 'constants'> ^ 2  ]
    UAConstants class >> iimDeregReq        [ <category: 'constants'> ^ 3  ]
    UAConstants class >> iimDeregResp       [ <category: 'constants'> ^ 4  ]

    UAConstants class >> tagReserved        [ <category: 'constants'> ^ 0  ]
    UAConstants class >> tagIdentInt        [ <category: 'constants'> ^ 1  ]
    UAConstants class >> tagUnused1         [ <category: 'constants'> ^ 2  ]
    UAConstants class >> tagIdentText       [ <category: 'constants'> ^ 3  ]
    UAConstants class >> tagInfo            [ <category: 'constants'> ^ 4  ]
    UAConstants class >> tagUnused2         [ <category: 'constants'> ^ 5  ]
    UAConstants class >> tagUnused3         [ <category: 'constants'> ^ 6  ]
    UAConstants class >> tagDiagInf         [ <category: 'constants'> ^ 7  ]
    UAConstants class >> tagIdentRange      [ <category: 'constants'> ^ 8  ]
    UAConstants class >> tagBeatData        [ <category: 'constants'> ^ 9  ]
    UAConstants class >> tagUnused4         [ <category: 'constants'> ^ 10 ]
    UAConstants class >> tagTraMode         [ <category: 'constants'> ^ 11 ]
    UAConstants class >> tagErrCode         [ <category: 'constants'> ^ 12 ]
    UAConstants class >> tagStatus          [ <category: 'constants'> ^ 13 ]
    UAConstants class >> tagUnused5         [ <category: 'constants'> ^ 14 ]
    UAConstants class >> tagUnused6         [ <category: 'constants'> ^ 15 ]
    UAConstants class >> tagUnused7         [ <category: 'constants'> ^ 16 ]
    UAConstants class >> tagAspIdent        [ <category: 'constants'> ^ 17 ]
    UAConstants class >> tagUnused8         [ <category: 'constants'> ^ 18 ]
    UAConstants class >> tagCorrelId        [ <category: 'constants'> ^ 19 ]

]


