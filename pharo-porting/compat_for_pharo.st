ByteArray extend [
    shortAt: index [
        <category: '*OsmoNetwork-Pharo'>

        "This is not signed right now"
        ^ self ushortAt: index
    ]

    ushortAt: index [
        <category: '*OsmoNetwork-Pharo'>

        ^ ((self at: index + 1) bitShift: 8) bitOr: (self at: index)
    ]

    uintAt: index [
        <category: '*OsmoNetwork-Pharo'>

        | byte1 byte2 byte3 byte4 |
        byte1 := (self at: index).
        byte2 := (self at: index + 1) bitShift:  8.
        byte3 := (self at: index + 2) bitShift: 16.
        byte4 := (self at: index + 3) bitShift: 24.

        ^ (((byte4 bitOr: byte3) bitOr: byte2) bitOr: byte1)
    ]
]

NotFound class extend [
    signalOn: anObject what: aMessage [
        <category: '*OsmoNetwork-Pharo'>
        ^ self new
            object: anObject;
            messageText: aMessage;
            signal 
    ]
]

Socket extend [
    ensureReadable [
        <category: '*OsmoNetwork-Pharo'>
        ^ self isValid
    ]

    isOpen [
        <category: '*OsmoNetwork-Pharo'>
        ^ self isConnected
    ]

    next [
        | data |
        <category: '*OsmoNetwork-Pharo'>
        data := ByteArray new: 2048.

        ">>#waitForData will block forever.. we use some form of polling"
        [self dataAvailable] whileFalse: [
            [self waitForDataFor: 10] on: ConnectionTimedOut do: []].
        self receiveUDPDataInto: data.
        ^data
    ]
]
