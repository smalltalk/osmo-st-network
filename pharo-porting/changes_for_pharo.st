"""
The following changes need to be kept for pharo.
"""
OsmoUDPSocketTest extend [
    createSocket [
        ^ Socket newUDP
    ]
]

OsmoUDPSocket extend [
    startRXProcess [
        ^ [[[
            Processor activeProcess name: name, ' RX'.
            self runRXProcess
            ] on: ConnectionClosed do: []
            ] ensure: [net_exit signal]] fork.
    ]
]

OsmoAppConnection extend [
    createConnection: aHostname port: aPort [
	<category: 'pharo-porting'>
	^(SocketStream openConnectionToHostNamed: aHostname port: aPort)
	    binary;
	    noTimeout;
	    yourself
    ]
]

OsmoStreamSocketBase extend [
    driveDispatch [
        <category: 'private'>
        [
            [self dispatchOne] on: ConnectionClosed do: [:e |
                self logError: 'OsmoApplication dispatch failed.' area: #osmo.
                self scheduleReconnect]
        ] on: Error do: [:e |
                e logException: 'OsmoApplication error' area: #osmo.
                self scheduleReconnect]
    ]

    driveSend [
        <category: 'private'>

        [
            self sendOne.
        ] on: ConnectionClosed do: [:e |
            e logException: 'OsmoAppConnection send failed' area: #osmo.
            self scheduleReconnect.
        ]
    ]
]

OsmoStreamSocketBase class extend [
    connectionException [
	<category: 'pharo-porting'>
	^ConnectionTimedOut
    ]
]
