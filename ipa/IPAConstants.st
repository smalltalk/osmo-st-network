"
 (C) 2010-2012 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

Object subclass: IPAConstants [
    <category: 'OsmoNetwork-IPA'>
    <comment: 'I have the constants for the IPA protocol.'>

    IPAConstants class >> protocolRSL  [ <category: 'constants'> ^ 16r00 ]
    IPAConstants class >> protocolMGCP [ <category: 'constants'> ^ 16rFC ]
    IPAConstants class >> protocolSCCP [ <category: 'constants'> ^ 16rFD ]
    IPAConstants class >> protocolIPA  [ <category: 'constants'> ^ 16rFE ]
    IPAConstants class >> protocolOML  [ <category: 'constants'> ^ 16rFF ]
    IPAConstants class >> protocolOSMO [ <category: 'constants'> ^ 16rEE ]

    IPAConstants class >> msgPing   [ <category: 'constants'> ^ 16r00 ]
    IPAConstants class >> msgPong   [ <category: 'constants'> ^ 16r01 ]
    IPAConstants class >> msgIdGet  [ <category: 'constants'> ^ 16r04 ]
    IPAConstants class >> msgIdResp [ <category: 'constants'> ^ 16r05 ]
    IPAConstants class >> msgIdAck  [ <category: 'constants'> ^ 16r06 ]
    IPAConstants class >> msgSCCP   [ <category: 'constants'> ^ 16rFF ]

    IPAConstants class >> idtagSernr     [ <category: 'constants'> ^ 16r00 ]
    IPAConstants class >> idtagUnitName  [ <category: 'constants'> ^ 16r01 ]
    IPAConstants class >> idtagLocation1 [ <category: 'constants'> ^ 16r02 ]
    IPAConstants class >> idtagLocation2 [ <category: 'constants'> ^ 16r03 ]
    IPAConstants class >> idtagEquipVer  [ <category: 'constants'> ^ 16r04 ]
    IPAConstants class >> idtagSwVersion [ <category: 'constants'> ^ 16r05 ]
    IPAConstants class >> idtagIpaddr    [ <category: 'constants'> ^ 16r06 ]
    IPAConstants class >> idtagMacaddr   [ <category: 'constants'> ^ 16r07 ]
    IPAConstants class >> idtagUnit      [ <category: 'constants'> ^ 16r08 ]

    IPAConstants class >> osmoCtrl      [ <category: 'osmo-extension-constants'> ^ 16r00 ]
    IPAConstants class >> osmoMgcp      [ <category: 'osmo-extension-constants'> ^ 16r01 ]
    IPAConstants class >> osmoLac       [ <category: 'osmo-extension-constants'> ^ 16r02 ]

    IPAConstants class >> protocolOsmoCTRL [
        <category: 'constants'>
        ^ {self protocolOSMO. self osmoCtrl}
    ]

    IPAConstants class >> protocolOsmoMGCP [
        <category: 'constants'>
        ^ {self protocolOSMO. self osmoMgcp}
    ]

    IPAConstants class >> protocolOsmoLAC [
        <category: 'constants'>
        ^ {self protocolOSMO. self osmoLac}
    ]
]
