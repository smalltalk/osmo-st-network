"
 (C) 2010-2013 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

CCompound subclass: CPackedStruct [
    <shape: #word>
    <category: 'OsmoNetwork-Core-GST'>
    <comment: 'I am a packed struct with one byte alignment,
    Paolo created me, Holger copied me here.'>

    CPackedStruct class >> declaration: array [
        "Compile methods that implement the declaration in array."

        <category: 'subclass creation'>
        self
            declaration: array
            inject: self superclass sizeof
            into: [:oldOffset :alignment | oldOffset]
    ]

    CPackedStruct class >> compileSize: size align: alignment [
        <category: 'private'>
        ^ super compileSize: size align: 1.
    ]
]

CPackedStruct subclass: IPASCCPState [
    <category: 'OsmoNetwork-IPA'>
    <comment: 'This is an Osmocom NAT extension to hand out USSD
    to a different provider.'>

    <declaration: #(
        #(#src  (#array #byte 3))
        #(#dst  (#array #byte 3))
        #(#transId #byte)
        #(#invokeId #byte)
        #(#imsi (#array #char 17))) >

    srcAddr [
        <category: 'accessing'>
        ^ SCCPAddrReference fromCData: self src.
    ]

    dstAddr [
        <category: 'accessing'>
        ^ SCCPAddrReference fromCData: self dst.
    ]

    imsiString [
        "We will need to count how many chars of the array are used"
        <category: 'accessing'>

        0 to: 16 do: [:index | | c |
            c := self imsi at: index.
            c = (Character value: 0)
                ifTrue: [
                    ^ String fromCData: self imsi size: index.
                ].
        ].

        ^ String fromCData: self imsi.
    ]
]
