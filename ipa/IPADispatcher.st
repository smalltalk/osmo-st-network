"
 (C) 2010 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

Object subclass: IPADispatcher [
    | handlers |

    <category: 'OsmoNetwork-IPA'>
    <comment: 'I am a hub and one can register handlers for the streams'>

    IPADispatcher class >> new [
        <category: 'creation'>
        ^self basicNew initialize
    ]

    initialize [
        <category: 'private'>
        handlers := Dictionary new.
    ]

    addHandler: aStream on: anObject with: aSelector [
        <category: 'handler'>
        handlers at: aStream put: [:msg | anObject perform: aSelector with: msg].
    ]

    addHandler: aStream on: aBlock [
        <category: 'handler'>
        handlers at: aStream put: aBlock.
    ]

    dispatch: aStream with: aData [
        | handler |
        <category: 'handler'>
        handler := handlers at: aStream ifAbsent: [
            self logError: ('IPADispatcher has no registered handler for <1p>'
                                expandMacrosWith: aStream) area: #ipa.
            ^ false
        ].

        handler value: aData.
    ]
]
