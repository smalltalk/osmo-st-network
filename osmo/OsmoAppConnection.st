"
 (C) 2011-2013 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

OsmoStreamSocketBase subclass: OsmoAppConnection [
    | writeQueue demuxer muxer dispatcher token connect_block |
    <category: 'OsmoNetwork-Socket'>
    <comment: 'I connect to a OpenBSC App on the Control Port and wait for
TRAPS coming from the server and will act on these.
TODO: re-use the IPADispatcher across connections.'>

    OsmoAppConnection class >> new [
        ^(self basicNew)
            hostname: '127.0.0.1';
            port: 4250;
            yourself
    ]

    initializeDispatcher [
        | ipa |
        "Allow another class to register handlers"
        dispatcher := IPADispatcher new.
        dispatcher initialize.
        connect_block ifNotNil: [connect_block value: dispatcher].
        ipa := (IPAProtoHandler new)
                    registerOn: dispatcher;
                    muxer: muxer;
                    token: token;
                    yourself
    ]

    nextPut: aData [
        muxer nextPut: aData with: IPAConstants protocolOsmoCTRL
    ]

    nextPut: aData with: aConstant [
        muxer nextPut: aData with: aConstant
    ]

    token: aToken [
        token := aToken.
    ]

    onConnect: aBlock [
        <category: 'creation'>
        "Call the block when the socket is being connected and the dispatcher
        is set-up. The callback will have the dispatcher as parameter."
        connect_block := aBlock.
    ]

    createConnection: aHostname port: aPort [
        <category: 'socket'>
        ^ Sockets.Socket remote: aHostname port: aPort.
    ]

    connect [
        <category: 'connect'>
        super connect.
        writeQueue := SharedQueue new.
        demuxer := IPADemuxer initOn: socket.
        muxer := IPAMuxer initOn: writeQueue.
        self initializeDispatcher.
    ]

    sendOne [
        | msg |
        <category: 'dispatch'>

        msg := writeQueue next.
        socket nextPutAllFlush: msg.
    ]

    dispatchOne [
        | msg |
        <category: 'dispatch'>

        msg := demuxer next.
        dispatcher dispatch: msg first with: msg second.
    ]
]

