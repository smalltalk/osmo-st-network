"I represent the logging areas"

Osmo.LogArea subclass: LogAreaCTRL [
    <category: 'OsmoNetwork-Control'>
    LogAreaCTRL class >> areaName [ ^ #ctrl ]
    LogAreaCTRL class >> areaDescription [ ^ 'Osmo CTRL handling' ]
    LogAreaCTRL class >> default [
        ^ self new
            enabled: true;
            minLevel: Osmo.LogLevel debug;
            yourself
    ]
]
