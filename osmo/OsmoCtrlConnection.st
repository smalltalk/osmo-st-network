"
 (C) 2011-2013 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

OsmoAppConnection subclass: OsmoCtrlConnection [
    | ctrlBlock |
    <category: 'OsmoNetwork-Socket'>

    onCtrlData: aBlock [
        <category: 'ctrl-dispatch'>
        ctrlBlock := aBlock
    ]

    handleCTRL: aCtrl [
        <category: 'ctrl-dispatch'>
        ctrlBlock value: aCtrl.
    ]

    initializeDispatcher [
        super initializeDispatcher.
        dispatcher
            addHandler: IPAConstants protocolOsmoCTRL
            on: self with: #handleCTRL:.
    ]

    sendCtrlData: aData [
        self nextPut: aData with: IPAConstants protocolOsmoCTRL
    ]
]
