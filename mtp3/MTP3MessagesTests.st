TestCase subclass: MTP3LabelTest [
    
    <comment: nil>
    <category: 'MTP3-Codec-Tests'>

    testParseFrom [
	| data stream label |
	data := #(16r01 16r80 16r00 16r00) asByteArray.
	stream := data readStream.
	label := MTP3Label parseFrom: stream.
	self assert: stream atEnd.
	self assert: label dpc = 1.
	self assert: label opc = 2.
	self assert: label slc = 0.
	self assert: label toMessage asByteArray = data
    ]
]



TestCase subclass: MTP3SLTAMSGTest [
    
    <comment: 'A MTP3SLTAMSGTest is a test class for testing the behavior of MTP3SLTAMSG'>
    <category: 'MTP3-Codec-Tests'>

    testParsing [
	| data stream msg |
	data := #(16r81 16r02 16r40 16r00 16r00 16r21 16rE0 16r47 16r53 16r4D 16r4D 16r4D 16r53 16r00 16r00 16r00 16r00 16r00 16r00 16r00 16r00) 
		    asByteArray.
	stream := data readStream.
	msg := MTP3MSG parseFrom: stream.
	self assert: stream atEnd.
	self assert: msg class = MTP3SLTAMSG.
	self assert: msg testPattern 
		    = #(16r47 16r53 16r4D 16r4D 16r4D 16r53 16r00 16r00 16r00 16r00 16r00 16r00 16r00 16r00) 
			    asByteArray.
	self assert: msg toMessage asByteArray = data
    ]
]



TestCase subclass: MTP3SLTMMSGTest [
    
    <comment: 'A MTP3SLTMMSGTest is a test class for testing the behavior of MTP3SLTMMSG'>
    <category: 'MTP3-Codec-Tests'>

    testParsing [
	| data stream msg |
	data := #(16r81 16r02 16r40 16r00 16r00 16r11 16rE0 16r47 16r53 16r4D 16r4D 16r4D 16r53 16r00 16r00 16r00 16r00 16r00 16r00 16r00 16r00) 
		    asByteArray.
	stream := data readStream.
	msg := MTP3MSG parseFrom: stream.
	self assert: stream atEnd.
	self assert: msg class = MTP3SLTMMSG.
	self assert: msg testPattern 
		    = #(16r47 16r53 16r4D 16r4D 16r4D 16r53 16r00 16r00 16r00 16r00 16r00 16r00 16r00 16r00) 
			    asByteArray.
	self assert: msg toMessage asByteArray = data
    ]
]



TestCase subclass: MTP3ServiceIndicatorsTest [
    
    <comment: 'A MTP3ServiceIndicatorsTest is a test class for testing the behavior of MTP3ServiceIndicators'>
    <category: 'MTP3-Codec-Tests'>

    testParseFrom [
	| data stream field |
	data := #(16r81) asByteArray.
	stream := data readStream.
	field := MTP3ServiceIndicators parseFrom: stream.
	self assert: stream atEnd.
	self assert: field subServiceField = MTP3ServiceIndicators nationalNetwork.
	self assert: field serviceIndicator 
		    = MTP3ServiceIndicators signallingNetworkTestingAndMaintenance.
	self assert: field toMessage asByteArray = data
    ]
]



TestCase subclass: MTP3HeadingTest [
    
    <comment: 'A MTP3HeadingTest is a test class for testing the behavior of MTP3Heading'>
    <category: 'MTP3-Codec-Tests'>

    testParsing [
	| data stream field |
	data := #(16r14) asByteArray.
	stream := data readStream.
	field := MTP3Heading parseFrom: stream.
	self assert: stream atEnd.
	self assert: field h0 = MTP3TFMMSG h0.
	self assert: field h1 = MTP3TFPMSG h1.
	self assert: field toMessage asByteArray = data
    ]
]

