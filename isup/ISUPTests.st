"
 (C) 2011-2012 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

TestCase subclass: ISUPGeneratedTest [
    <category: 'OsmoNetwork-Tests'>
    <comment: 'I was written on a flight to Taipei. I will try to
    instantiate all generated class and walk the hierachy to see if
    there are any DNUs'>

    playWith: aField [
        aField name.
        aField parameterValue.
        aField octalLength.
        self assert: aField isVarible ~= aField isFixed.

        aField isVarible ifTrue: [aField maxLength].
    ]

    testGeneration [
        ISUPMessage allSubclassesDo: [:class |
            | struct |
            struct := class tlvDescription.
            struct fixed do: [:each |
                self playWith: each].
            struct variable do: [:each |
                self playWith: each].
            struct optional do: [:each |
                self playWith: each].
            struct optionals do: [:each |
                self playWith: each].

            "same thing once more"
            struct fieldsDo: [:type :field_class |
                self playWith: field_class].
        ].
    ]

    testDecode [
        | decode struct data |
        decode := #(16r15 16r00 16r01 16r00 16r00 16r00 16r0A 16r00
                    16r02 16r0B 16r09 16r04 16r10 16r00 16r19 16r79
                    16r64 16r64 16r64 16r78 16r0A 16r09 16r02 16r13
                    16r00 16r79 16r51 16r20 16r01 16r79 16r42 16r00) asByteArray.

        struct := ISUPMessage decodeByteStream: decode readStream.

        data := ISUPMessage encodeCollection: struct.
        self assert: data asByteArray = decode.
    ]

    testClassCount [
        self assert: ISUPMessage allSubclasses size = 46.
    ]

    testSpecParmameterNameSmokeTest [
        MSGField allSubclassesDo: [:class |
            class category = 'OsmoNetwork-ISUP'
                ifTrue: [
                    self
                        assert: class parameterName isString;
                        assert: class spec isString]].
    ]
]
