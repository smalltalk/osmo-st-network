"
 (C) 2012 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

ISUPNatureOfConnectionIndicators class extend [
    satNoSat    [ <category: 'isup-extension'> ^ 2r00 ]
    satOneSat   [ <category: 'isup-extension'> ^ 2r01 ]
    satTwoSat   [ <category: 'isup-extension'> ^ 2r10 ]
    satSpare    [ <category: 'isup-extension'> ^ 2r11 ]

    cciNotRequired  [ <category: 'isup-extension'> ^ 2r00 ]
    cciRequired     [ <category: 'isup-extension'> ^ 2r01 ]
    cciPerformed    [ <category: 'isup-extension'> ^ 2r10 ]
    cciSpare        [ <category: 'isup-extension'> ^ 2r11 ]

    ecdiNotIncluded [ <category: 'isup-extension'> ^ 2r0 ]
    ecdiIncluded    [ <category: 'isup-extension'> ^ 2r1 ]
]

ISUPCallingPartysCategory class extend [
    callingSubscriberWithPriority [
	<category: 'encoding'>
	^2r1011
    ]

    categoryUnknown [
	<category: 'encoding'>
	^2r0
    ]

    dataCall [
	<category: 'encoding'>
	^2r1100
    ]

    operatorLanguageEnglish [
	<category: 'encoding'>
	^2r10
    ]

    operatorLanguageFrench [
	<category: 'encoding'>
	^2r1
    ]

    operatorLanguageGerman [
	<category: 'encoding'>
	^2r11
    ]

    operatorLanguageRussian [
	<category: 'encoding'>
	^2r100
    ]

    operatorLanguageSpanish [
	<category: 'encoding'>
	^2r101
    ]

    ordinarySubscriber [
	<category: 'encoding'>
	^2r1010
    ]

    payphone [
	<category: 'encoding'>
	^2r1111
    ]

    reserved [
	<category: 'encoding'>
	^2r1001
    ]

    spare [
	<category: 'encoding'>
	^2r1110
    ]

    testCall [
	<category: 'encoding'>
	^2r1101
    ]
]
