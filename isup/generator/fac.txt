#Facility
Message type 2.1 F 1
Message compatibility information 3.33 O 3-?
Parameter compatibility information 3.41 O 4-?
Remote operations (national use) 3.48 O 8-?
Service activation 3.49 O 3-?
Call transfer number (Note) 3.64 O 4-?
Access transport 3.3 O 3-?
Generic notification indicator 3.25 O 3
Redirection number 3.46 O 5-?
Pivot routing indicators 3.85 O 3
Pivot status (national use) 3.92 O 3
Pivot counter 3.93 O 3
Pivot routing backward information 3.95 O 3-?
Redirect status (national use) 3.98 O 3
End of optional parameters 3.20 O 1
