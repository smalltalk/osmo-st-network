Message type 2.1 F 1
Nature of connection indicators 3.35 F 1
Forward call indicators 3.23 F 2
Calling party's category 3.11 F 1
Transmission medium requirement 3.54 F 1
Called party number (Note 2) 3.9 V 4-?
Transit network selection (national use) 3.53 O 4-?
Call reference (national use) 3.8 O 7
Calling party number (Note 2) 3.10 O 4-?
Optional forward call indicators 3.38 O 3
Redirecting number (Note 2) 3.44 O 4-?
Redirection information 3.45 O 3-4
Closed user group interlock code 3.15 O 6
Connection request 3.17 O 7-9
Original called number (Note 2) 3.39 O 4-?
User-to-user information 3.61 O 3-131
Access transport 3.3 O 3-?
User service information 3.57 O 4-13
User-to-user indicators 3.60 O 3
Generic number (Notes 1 and 2) 3.26 O 5-?
Propagation delay counter 3.42 O 4
User service information prime 3.58 O 4-13
Network specific facility (national use) 3.36 O 4-?
Generic digits (national use) (Note 1) 3.24 O 4-?
Origination ISC point code 3.40 O 4
User teleservice information 3.59 O 4-5
Remote operations (national use) 3.48 O 8-?
Parameter compatibility information 3.41 O 4-?
Generic notification indicator (Note 1) 3.25 O 3
Service activation 3.49 O 3-?
Generic reference (reserved ) 3.27 O 5-?
MLPP precedence 3.34 O 8
Transmission medium requirement prime 3.55 O 3
Location number (Note 2) 3.30 O 4-?
Forward GVNS 3.66 O 5-26
CCSS 3.63 O 3-?
Network management controls 3.68 O 3-?
Circuit assignment map 3.69 O 6-7
Correlation id 3.70 O 3-?
Call diversion treatment indicators 3.72 O 3-?
Called IN number (Note 2) 3.73 O 4-?
Call offering treatment indicators 3.74 O 3-?
Conference treatment indicators 3.76 O 3-?
SCF id 3.71 O 3-?
UID capability indicators 3.79 O 3-?
Echo control information 3.19 O 3
Hop counter 3.80 O 3
Collect call request 3.81 O 3
Application transport parameter (Note 3) 3.82 O 5-?
Pivot capability 3.84 O 3
Called directory number (national use) 3.86 O 5-?
Original called IN number 3.87 O 4-?
Calling geodetic location 3.88 O 3-?
Network routing number (national use) 3.90 O 4-?
QoR capability (network option) 3.91 O 3
Pivot counter 3.93 O 3
Pivot routing forward information 3.94 O 3-?
Redirect capability (national use) 3.96 O 3
Redirect counter (national use) 3.97 O 3
Redirect status 3.98 O 3
Redirect forward information (national use) 3.99 O 3-?
Number portability forward information (network option) 3.101 O 1-?
End of optional parameters 3.20 O 1

