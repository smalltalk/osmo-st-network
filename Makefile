
GST_PACKAGE = gst-package
GST_CONVERT = gst-convert

CONVERT_RULES = -r'Osmo.LogManager->LogManager' \
		-r'Osmo.LogArea->LogArea'  \
		-r'Osmo.LogLevel->LogLevel' \
                -r'Osmo.TimerScheduler->TimerScheduler' \
                -r'Sockets.StreamSocket->SocketStream' \
		-r'DateTime->DateAndTime' \
		-r'(Duration milliseconds: ``@args1) -> (Duration milliSeconds: ``@args1)' \
		-r'PP.PPCompositeParser->PPCompositeParser' \
		-r'PP.PPCompositeParserTest->PPCompositeParserTest' \
		-r'STInST.RBProgramNodeVisitor->RBProgramNodeVisitor' \
		-r'STInST.RBBracketedMethodParser->RBParser' \
		-r'Osmo.MessageBuffer->MessageBuffer' \
		-r'SystemExceptions.NotFound->NotFound' \
		-r'(``@object substrings: ``@args1)->(``@object subStrings: ``@args1)' \
		-r'(Dictionary from: ``@args1)->(Dictionary newFrom: ``@args1)' \
		-r'(``@object copyFrom: ``@args1)->(``@object copyFrom: ``@args1 to: ``@object size)' \
		-r'(``@object nl)->(``@object cr; lf)' \
		-r'(``@object methodSourceString)->(``@object sourceCode)' \
		-C -IPAGSTTests

# Can not be parsed right now..
#		-r'(``@object => ``@args1)->(``@object ==> ``@args1)'

CORE = \
	core/Extensions.st core/MessageStructure.st core/MessageBuffer.st \
	core/LogAreas.st core/TLV.st core/TLVTests.st

IPA = \
	ipa/IPAConstants.st ipa/IPADispatcher.st ipa/IPAMuxer.st \
	ipa/IPAProtoHandler.st ipa/IPAMsg.st \

SCCP = \
        sccp/SCCP.st sccp/SCCPAddress.st \
        sccp/SCCPGlobalTitle.st sccp/SCCPGlobalTitleTranslation.st

ISUP = \
	isup/ISUP.st isup/isup_generated.st isup/ISUPExtensions.st \
	isup/ISUPTests.st

UA = \
	ua/XUA.st

M2UA = \
	m2ua/M2UAConstants.st m2ua/M2UAMSG.st m2ua/M2UATag.st m2ua/M2UAMessages.st \
	m2ua/M2UAStates.st m2ua/M2UAAspStateMachine.st \
        m2ua/M2UAApplicationServerProcess.st m2ua/M2UALayerManagement.st \
	m2ua/M2UAExamples.st m2ua/M2UATerminology.st m2ua/M2UATests.st 


OSMO = \
	osmo/LogAreaOsmo.st \
	osmo/OsmoUDPSocket.st osmo/OsmoCtrlLogging.st \
	osmo/OsmoStreamSocketBase.st \
	osmo/OsmoCtrlGrammar.st osmo/OsmoAppConnection.st \
	osmo/OsmoCtrlConnection.st osmo/OsmoCtrlGrammarTest.st

MTP3 = \
	mtp3/MTP3Messages.st mtp3/MTP3MessagesTests.st



all:
	$(GST_PACKAGE) --test package.xml

convert:
	$(GST_CONVERT) $(CONVERT_RULES) -F squeak -f gst \
		-o fileout.st pharo-porting/compat_for_pharo.st \
		$(CORE) $(IPA) $(SCCP) $(ISUP) $(UA) $(OSMO) $(MTP3) $(M2UA) \
		Tests.st pharo-porting/changes_for_pharo.st
	sed -i s,"=>","==>",g fileout.st

