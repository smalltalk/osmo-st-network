"
 (C) 2011-2013 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

UAConstants subclass: M2UAConstants [
    <category: 'OsmoNetwork-M2UA'>
    <comment: 'I hold the M2UA specific constants'>

    M2UAConstants class >> version            [ <category: 'constants'> ^ 1 ]
    M2UAConstants class >> spare              [ <category: 'constants'> ^ 0 ]


    M2UAConstants class >> tagData          [ <category: 'constants'> ^ 768 ]
    M2UAConstants class >> tagDataTTC       [ <category: 'constants'> ^ 769 ]
    M2UAConstants class >> tagStateReq      [ <category: 'constants'> ^ 770 ]
    M2UAConstants class >> tagStateEvent    [ <category: 'constants'> ^ 771 ]
    M2UAConstants class >> tagCongStatus    [ <category: 'constants'> ^ 772 ]
    M2UAConstants class >> tagDiscStatus    [ <category: 'constants'> ^ 773 ]
    M2UAConstants class >> tagAction        [ <category: 'constants'> ^ 774 ]
    M2UAConstants class >> tagSeqNo         [ <category: 'constants'> ^ 775 ]
    M2UAConstants class >> tagRetrRes       [ <category: 'constants'> ^ 776 ]
    M2UAConstants class >> tagLinkKey       [ <category: 'constants'> ^ 777 ]
    M2UAConstants class >> tagLocLinkeyIdent [ <category: 'constants'> ^ 778 ]
    M2UAConstants class >> tagSDT           [ <category: 'constants'> ^ 779 ]
    M2UAConstants class >> tagSDL           [ <category: 'constants'> ^ 780 ]
    M2UAConstants class >> tagRegRes        [ <category: 'constants'> ^ 781 ]
    M2UAConstants class >> tagRegStatus     [ <category: 'constants'> ^ 782 ]
    M2UAConstants class >> tagDeregRes      [ <category: 'constants'> ^ 783 ]
    M2UAConstants class >> tagDeregStatus   [ <category: 'constants'> ^ 784 ]

    M2UAConstants class >> statusLpoSet     [ <category: 'constants'> ^ 0  ]
    M2UAConstants class >> statusLpoClear   [ <category: 'constants'> ^ 1  ]
    M2UAConstants class >> statusEmergSet   [ <category: 'constants'> ^ 2  ]
    M2UAConstants class >> statusEmergClear [ <category: 'constants'> ^ 3  ]
    M2UAConstants class >> statusFlushBufs  [ <category: 'constants'> ^ 4  ]
    M2UAConstants class >> statusContinue   [ <category: 'constants'> ^ 5  ]
    M2UAConstants class >> statusClearRTB   [ <category: 'constants'> ^ 6  ]
    M2UAConstants class >> statusAudit      [ <category: 'constants'> ^ 7  ]
    M2UAConstants class >> statusCongCleared[ <category: 'constants'> ^ 8  ]
    M2UAConstants class >> statusCongAccept [ <category: 'constants'> ^ 9  ]
    M2UAConstants class >> statusCongDisc   [ <category: 'constants'> ^ 10 ]
    
    M2UAConstants class >> eventRPOEnter    [ <category: 'constants'> ^ 1 ]
    M2UAConstants class >> eventRPOExit     [ <category: 'constants'> ^ 2 ]
    M2UAConstants class >> eventLPOEnter    [ <category: 'constants'> ^ 3 ]
    M2UAConstants class >> eventLPOExit     [ <category: 'constants'> ^ 4 ]

    M2UAConstants class >> congLevelNone    [ <category: 'constants'> ^ 0 ]
    M2UAConstants class >> congLevel1       [ <category: 'constants'> ^ 1 ]
    M2UAConstants class >> congLevel2       [ <category: 'constants'> ^ 2 ]
    M2UAConstants class >> congLevel3       [ <category: 'constants'> ^ 3 ]

    M2UAConstants class >> actionRtrvBSN    [ <category: 'constants'> ^ 0 ]
    M2UAConstants class >> actionRtrvMSGs   [ <category: 'constants'> ^ 1 ]

    M2UAConstants class >> resultSuccess    [ <category: 'constants'> ^ 0 ]
    M2UAConstants class >> resultFailure    [ <category: 'constants'> ^ 1 ]

    M2UAConstants class >> traOverride      [ <category: 'constants'> ^ 1 ]
    M2UAConstants class >> traLoadShare     [ <category: 'constants'> ^ 2 ]
    M2UAConstants class >> traBroadcast     [ <category: 'constants'> ^ 3 ]

    M2UAConstants class >> errInvalidVersion    [ <category: 'constants'> ^ 1  ]
    M2UAConstants class >> errInvalidIdent      [ <category: 'constants'> ^ 2  ]
    M2UAConstants class >> errUnsMsgClass       [ <category: 'constants'> ^ 3  ]
    M2UAConstants class >> errUnsMsgType        [ <category: 'constants'> ^ 4  ]
    M2UAConstants class >> errUnsTraMode        [ <category: 'constants'> ^ 5  ]
    M2UAConstants class >> errUneMsg            [ <category: 'constants'> ^ 6  ]
    M2UAConstants class >> errProtocolError     [ <category: 'constants'> ^ 7  ]
    M2UAConstants class >> errUnsInterIdentInt  [ <category: 'constants'> ^ 8  ]
    M2UAConstants class >> errInvalidStreamIdent[ <category: 'constants'> ^ 9  ]
    M2UAConstants class >> errUnsued1           [ <category: 'constants'> ^ 10 ]
    M2UAConstants class >> errUnsued2           [ <category: 'constants'> ^ 11 ]
    M2UAConstants class >> errUnsued3           [ <category: 'constants'> ^ 12 ]
    M2UAConstants class >> errRefused           [ <category: 'constants'> ^ 13 ]
    M2UAConstants class >> errAspIdentRequired  [ <category: 'constants'> ^ 14 ]
    M2UAConstants class >> errInvalidAspIdent   [ <category: 'constants'> ^ 15 ]
    M2UAConstants class >> errAspActForIdent    [ <category: 'constants'> ^ 16 ]
    M2UAConstants class >> errInvalidParamVal   [ <category: 'constants'> ^ 17 ]
    M2UAConstants class >> errParamFieldError   [ <category: 'constants'> ^ 18 ]
    M2UAConstants class >> errUnexpParam        [ <category: 'constants'> ^ 19 ]
    M2UAConstants class >> errUnused4           [ <category: 'constants'> ^ 20 ] 
    M2UAConstants class >> errUnused5           [ <category: 'constants'> ^ 21 ]
    M2UAConstants class >> errMissingParam      [ <category: 'constants'> ^ 22 ]

    M2UAConstants class >> ntfyKindStateChange  [ <category: 'constants'> ^ 1 ]
    M2UAConstants class >> ntfyKindOther        [ <category: 'constants'> ^ 2 ]

    M2UAConstants class >> ntfyStateASInactive  [ <category: 'constants'> ^ 2 ]
    M2UAConstants class >> ntfyStateASActive    [ <category: 'constants'> ^ 3 ]
    M2UAConstants class >> ntfyStateASPending   [ <category: 'constants'> ^ 4 ]

    M2UAConstants class >> ntfyOtherInsuffRes   [ <category: 'constants'> ^ 1 ]
    M2UAConstants class >> ntfyOtherAltAspActiv [ <category: 'constants'> ^ 2 ]
    M2UAConstants class >> ntfyOtherAspFailure  [ <category: 'constants'> ^ 3 ]

    M2UAConstants class >> regSuccess           [ <category: 'constants'> ^ 0 ]
    M2UAConstants class >> regErrorUnknown      [ <category: 'constants'> ^ 1 ]
    M2UAConstants class >> regErrorInvSDLI      [ <category: 'constants'> ^ 2 ]
    M2UAConstants class >> regErrorInvSDTI      [ <category: 'constants'> ^ 3 ]
    M2UAConstants class >> regErrorInvLinkKey   [ <category: 'constants'> ^ 4 ]
    M2UAConstants class >> regErrorPermDenied   [ <category: 'constants'> ^ 5 ]
    M2UAConstants class >> regErrorOverlapKey   [ <category: 'constants'> ^ 6 ]
    M2UAConstants class >> regErrorNotProvisioned [ <category: 'constants'> ^ 7 ]
    M2UAConstants class >> regErrorInsuffRes    [ <category: 'constants'> ^ 8 ]

    M2UAConstants class >> deregSuccess         [ <category: 'constants'> ^ 0 ]
    M2UAConstants class >> deregErrorUnknown    [ <category: 'constants'> ^ 1 ]
    M2UAConstants class >> deregErrorInvIdent   [ <category: 'constants'> ^ 2 ]
    M2UAConstants class >> deregErrorPermDenied [ <category: 'constants'> ^ 3 ]
    M2UAConstants class >> deregErrorNotReg     [ <category: 'constants'> ^ 4 ]
]
