"
 (C) 2011-2013 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

M2UAMSG subclass: M2UAASPSMMessage [
    
    <category: 'OsmoNetwork-M2UA'>
    <comment: 'Application Server Process State Maintenance (ASPSM) messages'>

    M2UAASPSMMessage class >> messageClass [
	^M2UAConstants clsASPSM
    ]
]

M2UAMSG subclass: M2UAASPTMMessage [
    
    <category: 'OsmoNetwork-M2UA'>
    <comment: nil>

    M2UAASPTMMessage class >> messageClass [
	^M2UAConstants clsASPTM
    ]
]

M2UAMSG subclass: M2UAASPMGMTMessage [
    
    <category: 'OsmoNetwork-M2UA'>
    <comment: nil>

    M2UAASPMGMTMessage class >> messageClass [
	^M2UAConstants clsMgmt
    ]
]

M2UAASPSMMessage subclass: M2UAApplicationServerProcessHeartbeatAck [
    
    <category: 'OsmoNetwork-M2UA'>
    <comment: nil>

    M2UAApplicationServerProcessHeartbeatAck class >> messageTag [
	^M2UAConstants aspsmBeatAck
    ]
]

M2UAASPSMMessage subclass: M2UAApplicationServerProcessDown [
    
    <category: 'OsmoNetwork-M2UA'>
    <comment: nil>

    M2UAApplicationServerProcessDown class >> messageTag [
	^M2UAConstants aspsmDown
    ]

    dispatchOnAsp: anAsp [
	<category: 'm2ua-asp-dispatch'>
	anAsp handleAspDown: self
    ]
]

M2UAASPSMMessage subclass: M2UAApplicationServerProcessHeartbeat [
    
    <category: 'OsmoNetwork-M2UA'>
    <comment: nil>

    M2UAApplicationServerProcessHeartbeat class >> messageTag [
	^M2UAConstants aspsmBeat
    ]
]

M2UAASPSMMessage subclass: M2UAApplicationServerProcessDownAck [
    
    <category: 'OsmoNetwork-M2UA'>
    <comment: nil>

    M2UAApplicationServerProcessDownAck class >> messageTag [
	^M2UAConstants aspsmDownAck
    ]

    dispatchOnAsp: anAsp [
	<category: 'm2ua-asp-dispatch'>
	anAsp handleAspDownAck: self
    ]
]

M2UAASPSMMessage subclass: M2UAApplicationServerProcessUp [
    
    <category: 'OsmoNetwork-M2UA'>
    <comment: nil>

    M2UAApplicationServerProcessUp class >> messageTag [
	^M2UAConstants aspsmUp
    ]

    dispatchOnAsp: anAsp [
	<category: 'm2ua-asp-dispatch'>
	anAsp handleAspUp: self
    ]
]

M2UAASPTMMessage subclass: M2UAApplicationServerProcessInactiveAck [
    
    <category: 'OsmoNetwork-M2UA'>
    <comment: nil>

    M2UAApplicationServerProcessInactiveAck class >> messageTag [
	^M2UAConstants asptmInactivAck
    ]

    dispatchOnAsp: anAsp [
	<category: 'm2ua-asp-dispatch'>
	anAsp handleAspInactiveAck: self
    ]
]

M2UAASPTMMessage subclass: M2UAApplicationServerProcessActive [
    
    <category: 'OsmoNetwork-M2UA'>
    <comment: nil>

    M2UAApplicationServerProcessActive class >> messageTag [
	^M2UAConstants asptmActiv
    ]

    dispatchOnAsp: anAsp [
	<category: 'm2ua-asp-dispatch'>
	anAsp handleAspActive: self
    ]
]

M2UAASPTMMessage subclass: M2UAApplicationServerProcessInactive [
    
    <category: 'OsmoNetwork-M2UA'>
    <comment: nil>

    M2UAApplicationServerProcessInactive class >> messageTag [
	^M2UAConstants asptmInactiv
    ]

    dispatchOnAsp: anAsp [
	<category: 'm2ua-asp-dispatch'>
	anAsp handleAspInactive: self
    ]
]

M2UAASPMGMTMessage subclass: M2UAApplicationServerProcessNotify [
    
    <category: 'OsmoNetwork-M2UA'>
    <comment: nil>

    M2UAApplicationServerProcessNotify class >> messageTag [
	^M2UAConstants mgmtNtfy
    ]

    dispatchOnAsp: anAsp [
	<category: 'm2ua-asp-dispatch'>
	anAsp handleNotify: self
    ]
]

M2UAASPMGMTMessage subclass: M2UAApplicationServerProcessError [
    
    <category: 'OsmoNetwork-M2UA'>
    <comment: nil>

    M2UAApplicationServerProcessError class >> messageTag [
	^M2UAConstants mgmtError
    ]

    dispatchOnAsp: anAsp [
	<category: 'm2ua-asp-dispatch'>
	anAsp handleError: self
    ]
]

M2UAASPTMMessage subclass: M2UAApplicationServerProcessActiveAck [
    
    <category: 'OsmoNetwork-M2UA'>
    <comment: nil>

    M2UAApplicationServerProcessActiveAck class >> messageTag [
	^M2UAConstants asptmActivAck
    ]

    dispatchOnAsp: anAsp [
	<category: 'm2ua-asp-dispatch'>
	anAsp handleAspActiveAck: self
    ]
]

M2UAASPSMMessage subclass: M2UAApplicationServerProcessUpAck [
    
    <category: 'OsmoNetwork-M2UA'>
    <comment: nil>

    M2UAApplicationServerProcessUpAck class >> messageTag [
	^M2UAConstants aspsmUpAck
    ]

    dispatchOnAsp: anAsp [
	<category: 'm2ua-asp-dispatch'>
	anAsp handleAspUpAck: self
    ]
]
