"
 (C) 2011-2013 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

Object subclass: M2UATag [
    | tag_nr data |

    <category: 'OsmoNetwork-M2UA'>
    <comment: 'I represent a tag of a M2UA packet. I hold the
    number of the tag and the data associated with it.'>

    M2UATag class >> fromStream: aStream [
        <category: 'parsing'>
        ^ self new
            parseFrom: aStream
    ]

    M2UATag class >> initWith: aTag data: aData [
        <category: 'creation'>
        ^ self new
            instVarNamed: #tag_nr put: aTag;
            instVarNamed: #data put: aData;
            yourself
    ]

    parseFrom: aStream [
        | len padding |
        <category: 'parsing'>

        tag_nr := ((aStream next: 2) shortAt: 1) swap16.
        len := ((aStream next: 2) shortAt: 1) swap16.
        data := aStream next: len - 4.

        padding := len \\ 4.
        padding > 0 ifTrue: [
            self logNotice:
                ('Going to skip <1p> bytes' expandMacrosWith: 4 - padding) area: #m2ua.
            aStream skip: 4 - padding.
        ].
    ]

    nr [
        <category: 'accessing'>
        ^ tag_nr
    ]

    data [
        <category: 'accessing'>
        ^ data ifNil: [data := ByteArray new]
    ]

    writeOn: aMsg [
        | rest |
        <category: 'private'>

        aMsg putLen16: tag_nr.
        aMsg putLen16: self data size + 4.
        aMsg putByteArray: self data.

        rest := self data size \\ 4.
        rest > 0 ifTrue: [
            aMsg putByteArray: (ByteArray new: 4 - rest).
        ].
    ]

    isTag: aNr [
        <category: 'accessing'>
        ^ self nr = aNr
    ]
]

