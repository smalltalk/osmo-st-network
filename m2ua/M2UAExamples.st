"
 (C) 2013 by Holger Hans Peter Freyther
 All Rights Reserved

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"

Object subclass: M2UAExamples [
    
    <category: 'OsmoNetwork-M2UA'>
    <comment: nil>

    createAsp [
	"Create a SCTP network service"

	| service asp manager |
	service := SCTPNetworkService new
		    hostname: 'localhost';
		    port: 2904;
		    yourself.
	"Create the ASP"
	asp := M2UAApplicationServerProcess initWith: service.

	"Create a Layer Management (LM) and start it"
	manager := M2UALayerManagement new
		    applicationServerProcess: asp;
		    targetState: M2UAAspStateActive;
		    yourself.
	manager manage
    ]
]
